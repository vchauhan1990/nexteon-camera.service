package com.nexteon.camera.services.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nexteon.camera.services.common.HttpPutRequest;
import com.nexteon.camera.services.entities.CameraSetting;
import com.nexteon.camera.services.entities.ResponseStatus;

@RestController
public class BaseController {

	@GetMapping("/bin/election/camera/setting")
	public @ResponseBody ResponseStatus getDemoRequest(@RequestBody CameraSetting camSetting) {
		ResponseStatus res = new ResponseStatus();
		boolean status = false;
		try {
			if(!camSetting.getCamid().equals("")) {
				if(!camSetting.getCamip().equals("")) {
					String IP = camSetting.getCamip();
					if(!camSetting.getSharpness().equals("")) {
						String response = new HttpPutRequest().getReadResponse("http://"+IP+"/ISAPI/Image/channels/1/sharpness", 
								"<Sharpness><SharpnessLevel>"+camSetting.getSharpness()+"</SharpnessLevel></Sharpness>", "PUT");
						if(response.contains("OK")) {
							res.setStatusCode("1");
							res.setStatusString("OK");
							res.setStatusMsg("OK");
						}
					}
				}else {
					res.setStatusCode("3");
					res.setStatusString("ERROR");
					res.setStatusMsg("CAMERA IP CANNOT BE NULL OR EMPTY");
				}
			}else {
				res.setStatusCode("2");
				res.setStatusString("ERROR");
				res.setStatusMsg("CAMERA ID CANNOT BE NULL OR EMPTY");
			}
		} catch (Exception e) {
			res.setStatusCode("10");
			res.setStatusString("ERROR");
			res.setStatusMsg(e.getMessage());
		}
		return res;
	}

}
