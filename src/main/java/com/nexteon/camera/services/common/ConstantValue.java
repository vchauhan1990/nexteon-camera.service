package com.nexteon.camera.services.common;

public enum ConstantValue {

	USERNAME("admin"),
	PASSWORD("Ver1c084"),
	SHARPNESSAPIURL("/ISAPI/Image/channels/1/sharpness");
	
	private String value;  
	
	private ConstantValue(String value){  
		this.value=value;  
	}  
	
	public String getValue() {
        return this.value;
    }
	
	@Override
	public String toString() {
	        return this.value;
	}
	
}
