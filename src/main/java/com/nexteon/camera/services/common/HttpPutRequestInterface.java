package com.nexteon.camera.services.common;

import java.io.IOException;

public interface HttpPutRequestInterface {

	public String getReadResponse(String URL, String data, String requestType) throws IOException;
	
}
