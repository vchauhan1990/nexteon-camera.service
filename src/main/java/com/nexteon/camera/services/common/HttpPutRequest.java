package com.nexteon.camera.services.common;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AUTH;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.DigestScheme;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;

import com.nexteon.camera.services.entities.ResponseStatus;

@Service
public class HttpPutRequest implements HttpPutRequestInterface {

	@Override
	public String getReadResponse(String urlValue, String data, String requestType) throws IOException {
		ResponseStatus res = new ResponseStatus();
		try {
			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpClient httpclient2 = HttpClientBuilder.create().build();
			
			HttpResponse r = null;
			HttpPut httpput = null;
			HttpGet httpget = null;
			if(requestType.equals("PUT")) {
				httpput = new HttpPut(urlValue);
				httpput.setEntity(new StringEntity(data));
				r = httpclient.execute(httpput);
			}else if(requestType.equals("GET")) {
				httpget = new HttpGet(urlValue);
				r = httpclient.execute(httpget);
			}
			

			if (r.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
				Header authHeader = r.getFirstHeader(AUTH.WWW_AUTH);
				DigestScheme digestScheme = new DigestScheme();
				digestScheme.processChallenge(authHeader);
				UsernamePasswordCredentials creds = new UsernamePasswordCredentials(ConstantValue.USERNAME.toString(),ConstantValue.PASSWORD.toString());
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				String responseBody = "";
				if(requestType.equals("PUT")) {
					httpput.addHeader(digestScheme.authenticate(creds, httpput));
					responseBody = httpclient2.execute(httpput, responseHandler);
				}else if(requestType.equals("GET")) {
					httpget.addHeader(digestScheme.authenticate(creds, httpget));
					responseBody = httpclient2.execute(httpput, responseHandler);
				}
				return responseBody;
			}
		} catch (Exception e) {
			res.setStatusMsg("MalformedURLException" + e);
		}
		return null;
	}

}
