package com.nexteon.camera.services.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CameraSetting")
@XmlAccessorType(XmlAccessType.FIELD)
public class CameraSetting implements Serializable{

	private static final long serialVersionUID = -1566799268659434067L;

	@XmlElement(required = true)
	private String camid;
	
	@XmlElement(required = true)
	private String camip;
	
	@XmlElement
	private String quality;
	
	@XmlElement
	private String audio;
	
	@XmlElement
	private String sharpness;
	
	@XmlElement
	private String brightness;
	
	@XmlElement
	private String contrast;
	
	@XmlElement
	private String saturation;
	
	@XmlElement
	private String wdr;
	
	@XmlElement
	private String ircut;
	
	@XmlElement
	private String flip;
	
	@XmlElement
	private String mirror;

	public String getCamid() {
		return camid;
	}

	public void setCamid(String camid) {
		this.camid = camid;
	}

	public String getCamip() {
		return camip;
	}

	public void setCamip(String camip) {
		this.camip = camip;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public String getAudio() {
		return audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}

	public String getSharpness() {
		return sharpness;
	}

	public void setSharpness(String sharpness) {
		this.sharpness = sharpness;
	}

	public String getBrightness() {
		return brightness;
	}

	public void setBrightness(String brightness) {
		this.brightness = brightness;
	}

	public String getContrast() {
		return contrast;
	}

	public void setContrast(String contrast) {
		this.contrast = contrast;
	}

	public String getSaturation() {
		return saturation;
	}

	public void setSaturation(String saturation) {
		this.saturation = saturation;
	}

	public String getWdr() {
		return wdr;
	}

	public void setWdr(String wdr) {
		this.wdr = wdr;
	}

	public String getIrcut() {
		return ircut;
	}

	public void setIrcut(String ircut) {
		this.ircut = ircut;
	}

	public String getFlip() {
		return flip;
	}

	public void setFlip(String flip) {
		this.flip = flip;
	}

	public String getMirror() {
		return mirror;
	}

	public void setMirror(String mirror) {
		this.mirror = mirror;
	}

	@Override
	public String toString() {
		return "CameraSetting [camid=" + camid + ", camip=" + camip + ", quality=" + quality + ", audio=" + audio
				+ ", sharpness=" + sharpness + ", brightness=" + brightness + ", contrast=" + contrast + ", saturation="
				+ saturation + ", wdr=" + wdr + ", ircut=" + ircut + ", flip=" + flip + ", mirror=" + mirror + "]";
	}
	
	
}
