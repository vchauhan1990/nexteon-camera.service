package com.nexteon.camera.services.entities;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "StreamingChannel")
public class StreamingChannel implements Serializable{

	private static final long serialVersionUID = -4241797754744455953L;

	@XmlElement
	private String id;
	
	@XmlElement
	private String channelName;
	
	@XmlElement
	private boolean enabled;
	
	@XmlElement
	private List<Video> videoList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<Video> getVideoList() {
		return videoList;
	}

	public void setVideoList(List<Video> videoList) {
		this.videoList = videoList;
	}

	@Override
	public String toString() {
		return "StreamingChannel [id=" + id + ", channelName=" + channelName + ", enabled=" + enabled + ", videoList="
				+ videoList + "]";
	}
	
	
	
	
}
