package com.nexteon.camera.services.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "StreamingChannel")
public class Video implements Serializable{

	private static final long serialVersionUID = 2389846269108965030L;

	@XmlElement
	private boolean enabled;

	@XmlElement
	private String videoResolutionWidth;
	
	@XmlElement
	private String videoResolutionHeight;
	
	@XmlElement
	private String maxFrameRate;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getVideoResolutionWidth() {
		return videoResolutionWidth;
	}

	public void setVideoResolutionWidth(String videoResolutionWidth) {
		this.videoResolutionWidth = videoResolutionWidth;
	}

	public String getVideoResolutionHeight() {
		return videoResolutionHeight;
	}

	public void setVideoResolutionHeight(String videoResolutionHeight) {
		this.videoResolutionHeight = videoResolutionHeight;
	}

	public String getMaxFrameRate() {
		return maxFrameRate;
	}

	public void setMaxFrameRate(String maxFrameRate) {
		this.maxFrameRate = maxFrameRate;
	}

	@Override
	public String toString() {
		return "Video [enabled=" + enabled + ", videoResolutionWidth=" + videoResolutionWidth
				+ ", videoResolutionHeight=" + videoResolutionHeight + ", maxFrameRate=" + maxFrameRate + "]";
	}
	
	
	
	
}
