package com.nexteon.camera.services.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement (name = "ResponseStatus")
@XmlAccessorType(XmlAccessType.NONE)
public class ResponseStatus implements Serializable{

	private static final long serialVersionUID = -4824016399586974111L;
	
	 @XmlAttribute
	 private String version;
	 
	 @XmlElement
	 private String requestURL;
	
	 @XmlElement
	 private String statusCode;
	 
	 @XmlElement
	 private String statusString;
	 
	 @XmlElement
	 private String statusMsg;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getRequestURL() {
		return requestURL;
	}

	public void setRequestURL(String requestURL) {
		this.requestURL = requestURL;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusString() {
		return statusString;
	}

	public void setStatusString(String statusString) {
		this.statusString = statusString;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	@Override
	public String toString() {
		return "ResponseStatus [version=" + version + ", requestURL=" + requestURL + ", statusCode=" + statusCode
				+ ", statusString=" + statusString + ", statusMsg=" + statusMsg + "]";
	}
	
}
